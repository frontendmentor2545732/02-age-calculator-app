import dayjs from 'dayjs'
import preciseDiff from 'dayjs-precise-range'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)
dayjs.extend(preciseDiff)

document.querySelector('form').addEventListener('submit', (event) => {
    event.preventDefault()
    event.stopPropagation()

    const form = event.target

    let formValid = validateYear(form)
    formValid = validateMonth(form) && formValid
    formValid = validateDay(form) && formValid

    if (formValid) {
        updateAge(form)
    }
})

function validateYear(form) {
    const yearInput = form.elements.year
    if (!validateInput(yearInput)) {
        return false
    }
    const errorElement = yearInput.previousElementSibling
    const currentYear = new Date().getFullYear()
    if (currentYear <= yearInput.value) {
        const errorMessage = 'Must be in the past'
        yearInput.setCustomValidity(errorMessage)
        errorElement.textContent = errorMessage
        return false
    }
    return true
}

function validateMonth(form) {
    return validateInput(form.elements.month)
}

function validateDay(form) {
    const dayInput = form.elements.day
    if (!validateInput(dayInput)) {
        return false
    }
    const errorElement = dayInput.previousElementSibling
    if (!constructDate(form).isValid()) {
        const errorMessage = 'Must be a valid date'
        dayInput.setCustomValidity(errorMessage)
        errorElement.textContent = errorMessage
        return false
    }
    return true
}

function validateInput(input) {
    const errorElement = input.previousElementSibling
    if (input.validity.valueMissing) {
        errorElement.textContent = 'This field is required'
        return
    }
    if (
        input.validity.badInput ||
        input.validity.stepMismatch ||
        input.validity.rangeOverflow ||
        input.validity.rangeUnderflow
    ) {
        errorElement.textContent = `Must be a valid ${input.name}`
        return false
    }
    input.setCustomValidity('')
    errorElement.textContent = ''
    return true
}

function updateAge(form) {
    const { years, months, days } = dayjs.preciseDiff(
        new Date(),
        constructDate(form),
        true
    )
    animateValue(document.getElementById('years'), 0, years, 1000)
    animateValue(document.getElementById('months'), 0, months, 1000)
    animateValue(document.getElementById('days'), 0, days, 1000)
}

function constructDate(form) {
    const dayInput = form.elements.day
    const monthInput = form.elements.month
    const yearInput = form.elements.year
    const dateString = `${yearInput.value.padStart(
        4,
        '0'
    )}-${monthInput.value.padStart(2, '0')}-${dayInput.value.padStart(2, '0')}`
    return dayjs(dateString, 'YYYY-MM-DD', true)
}

function animateValue(obj, start, end, duration) {
    let startTimestamp = null
    const step = (timestamp) => {
        if (!startTimestamp) startTimestamp = timestamp
        const progress = Math.min((timestamp - startTimestamp) / duration, 1)
        obj.textContent = Math.floor(progress * (end - start) + start)
        if (progress < 1) {
            window.requestAnimationFrame(step)
        }
    }
    window.requestAnimationFrame(step)
}
