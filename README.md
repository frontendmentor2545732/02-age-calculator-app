# Frontend Mentor - Age calculator app solution

This is a solution to the [Age calculator app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/age-calculator-app-dF9DFFpj-Q). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

-   [Overview](#overview)
    -   [The challenge](#the-challenge)
    -   [Screenshot](#screenshot)
    -   [Links](#links)
-   [My process](#my-process)
    -   [Built with](#built-with)
    -   [What I learned](#what-i-learned)
    -   [Continued development](#continued-development)
    -   [Useful resources](#useful-resources)

## Overview

### The challenge

Users should be able to:

-   View an age in years, months, and days after submitting a valid date through the form
-   Receive validation errors if:
    -   Any field is empty when the form is submitted
    -   The day number is not between 1-31
    -   The month number is not between 1-12
    -   The year is in the future
    -   The date is invalid e.g. 31/04/1991 (there are 30 days in April)
-   View the optimal layout for the interface depending on their device's screen size
-   See hover and focus states for all interactive elements on the page
-   **Bonus**: See the age numbers animate to their final number when the form is submitted

### Screenshot

![](./screenshot.jpg)

### Links

-   Solution URL: [Gitlab Repo] solution URL here](https://gitlab.com/frontendmentor2545732/02-age-calculator-app)
-   Live Site URL: [Live Site](https://frontendmentor2545732.gitlab.io/02-age-calculator-app/)

## My process

### Built with

-   Modern CSS including nested queries and custom media queries
-   CSS Grid for for the high-level layout
-   Flexbox, mostly for spacing
-   [Vite](https://vitejs.dev/) - To build everything
-   [PostCSS](https://postcss.org/) - Convert modern CSS to CSS that runs in current browsers
-   [DayJS](https://day.js.org/) - A tiny date library to validate date
-   [DayJS Precise Range](https://github.com/huangjinlin/dayjs-precise-range) - A plugin to subtract dates in the format requested

### What I learned

-   Form validation in Vanilla JS. Usually I would use a form library, but I deliberately wanted to see how to do this without one.
-   I opted for a tiny library to validate and substract dates. That isn't a wheel I felt I needed to reinvent.
-   After playing around with UnoCSS for the last few challenges, I wanted to build this one with regular, modern CSS. I also haven't used PostCSS before. When you can use all the modern features without worrying too much about browser support, CSS isn't half bad, actually.

### Useful resources

-   [MDN: Client-side form validation](https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation) - MDN docs on form validation with vanilla JS
-   [CSS-Tricks: Animating Number Counters](https://css-tricks.com/animating-number-counters/#the-new-school-css-solution) - I went with their JS solution
